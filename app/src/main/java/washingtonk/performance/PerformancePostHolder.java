package washingtonk.performance;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class PerformancePostHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public PerformancePostHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
