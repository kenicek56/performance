package washingtonk.performance;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallback {
    public ArrayList<todoitem> performancePosts = new ArrayList<>();
    public int currentItem;

    @Override
    protected Fragment createFragment() {
        return new PerformanceListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }


    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new ItemViewFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}