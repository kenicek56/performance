package washingtonk.performance;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class PerformancePostAdapter extends RecyclerView.Adapter<PerformancePostHolder> {

    private ArrayList<todoitem> todoitem;
    private ActivityCallback activityCallback;

    public PerformancePostAdapter (ActivityCallback activityCallback, ArrayList<todoitem> todoitems) {
        this.activityCallback = activityCallback;
        this.todoitem = todoitems;
    }

    @Override
    public PerformancePostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new PerformancePostHolder(view);
    }

    @Override
    public void onBindViewHolder(PerformancePostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCallback.onPostSelected(todoitemUri);
                activityCallback.onPostSelected(position);
            }
        });
        holder.titleText.setText(todoitem.get(position).title);

    }

    @Override
    public int getItemCount() {
        return todoitem.size();
    }

}
