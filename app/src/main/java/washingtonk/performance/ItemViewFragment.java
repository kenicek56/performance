package washingtonk.performance;

import android.app.Fragment;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class ItemViewFragment extends Fragment{
    private RecyclerView recyclerView;
    private MainActivity activity;
    private ActivityCallback activityCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.view_fragment, container, false);

        TextView tv1 = (TextView)layoutView.findViewById(R.id.view1);
        TextView tv2 = (TextView)layoutView.findViewById(R.id.view2);
        TextView tv3 = (TextView)layoutView.findViewById(R.id.view3);
        TextView tv4 = (TextView)layoutView.findViewById(R.id.view4);

        tv1.setText("Title: " + activity.performancePosts.get(activity.currentItem).title);
        tv2.setText("Chores: " + activity.performancePosts.get(activity.currentItem).chores);
        tv3.setText("Date: " + activity.performancePosts.get(activity.currentItem).dateDue);
        tv4.setText("Data: " + activity.performancePosts.get(activity.currentItem).dataAdded);

        return layoutView;
    }
}

