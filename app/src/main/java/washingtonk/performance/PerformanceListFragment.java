package washingtonk.performance;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PerformanceListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback) activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_performance_list,container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        todoitem todoitem1 = new todoitem("titles", "2","3","4");
        todoitem todoitem2 = new todoitem("chores", "2", "3", "4");
        todoitem todoitem3 = new todoitem("dataAdded", "2", "3","4");
        todoitem todoitem4 = new todoitem("dateDue", "2", "3", "4");


        activity.performancePosts.add(todoitem1);
        activity.performancePosts.add(todoitem2);
        activity.performancePosts.add(todoitem3);
        activity.performancePosts.add(todoitem4);


        PerformancePostAdapter adapter = new PerformancePostAdapter(activityCallback, activity.performancePosts);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
